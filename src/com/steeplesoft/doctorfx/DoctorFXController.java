/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.steeplesoft.doctorfx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.AttributesBuilder;
import org.asciidoctor.OptionsBuilder;
import org.asciidoctor.SafeMode;

/**
 *
 * @author Jason Lee <jason@steeplesoft.com>
 */
public class DoctorFXController implements Initializable {
    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private String filePath;
    private final Asciidoctor instance;
    private Map<String, Object> options;
    @FXML
    TextArea editField;
    @FXML
    WebView preview;
    @FXML
    TextArea renderedSource;

    public DoctorFXController() {
        options = OptionsBuilder.options()
                .compact(false)
                .headerFooter(true)
                .safe(SafeMode.UNSAFE)
                .backend("html")
                .attributes(AttributesBuilder.attributes().imagesDir(new File(".")).asMap())
                .asMap();
        instance = Asciidoctor.Factory.create();
    }

    @FXML
    protected void handleKeyTyped(KeyEvent event) {
        updatePreview();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    public void openFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Show open file dialog
        String newFilePath = fileChooser.showOpenDialog(null).getAbsolutePath();
        System.out.println(newFilePath);
        try {
            String text = new Scanner( new File(newFilePath), "UTF-8" ).useDelimiter("\\A").next();
            editField.setText(text);
            filePath = newFilePath;
            updatePreview();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DoctorFXController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void saveFile(ActionEvent event) {
        try (PrintWriter out = new PrintWriter(filePath)) {
            out.write(editField.getText());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DoctorFXController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void copyText(ActionEvent event) {
        final ClipboardContent content = new ClipboardContent();
        content.putString(editField.getSelectedText());
        clipboard.setContent(content);
    }
    
    @FXML
    public void cutText(ActionEvent event) {
        final ClipboardContent content = new ClipboardContent();
        content.putString(editField.getSelectedText());
        clipboard.setContent(content);
        editField.deleteText(editField.getSelection());
    }
    
    @FXML
    public void pasteText(ActionEvent event) {
        editField.insertText(editField.getCaretPosition(), clipboard.getString());
    }
    
    @FXML
    public void deleteText(ActionEvent event) {
        editField.deleteText(editField.getSelection());
    }
    
    @FXML
    public void selectAll(ActionEvent event) {
        editField.selectAll();
    }
    
    // TODO: Binding might be nice here, though we might not be able to throttle the update rate
    protected void updatePreview() {
        final String rendered = instance.render(editField.getText(), options);
        preview.getEngine().loadContent(rendered);
        renderedSource.setText(rendered);
    }
}
